package org.ape.connectors;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.ape.commons.BlackboxToolConnector;
import org.ape.commons.Vulnerability;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class SkipfishConnector implements BlackboxToolConnector {
	
	private static final String ROOT="/home/backbox/Tese/blackbox-tools/skipfish-2.10b/";
	private static final String EXECUTABLE_PATH = ROOT + "skipfish";
	private static final String[] READ_DICTIONARIES = { ROOT+"dictionaries/minimal.wl"};
	private static final String WRITE_DICTIONARY = ROOT+"dictionaries/wackopicko.wl";
	
	private static final String OUTPUT_FOLDER = "/home/eduardo/tese/blackbox-tools/outputs/skipfish";
	
	private static final String TARGET_URL = "http://localhost";

	List<Vulnerability> result;
	
    /**
     * Default constructor. 
     */
    public SkipfishConnector() {
       result = new LinkedList<Vulnerability>();
       
    }
    
    

	@Override
	public void execute() throws InterruptedException, IOException {
System.out.println("Executing skipfish...");
		
		List<String> args = new ArrayList<String>(7 + READ_DICTIONARIES.length);
		args.add(EXECUTABLE_PATH);
		
		//Quiet-mode
		args.add("-u");
		
		//Read-only Dictionaries
		args.add("-S");
		for(String s : READ_DICTIONARIES)
			args.add(s);
		
		args.add("-W");
		args.add(WRITE_DICTIONARY);
		
		args.add("-o");
		args.add(OUTPUT_FOLDER);
		
		args.add(TARGET_URL);
		
		
		
		
		ProcessBuilder pb = new ProcessBuilder(args);
		pb.directory(new File(ROOT));
	 // pb.redirectOutput(Redirect.INHERIT);
	    pb.redirectError(Redirect.INHERIT);
	    pb.start().waitFor();
	    System.out.println("Skipfish has finished scanning.");
	    getFoundVuln();
	}

	private int getFoundVuln() {
		File f = new File(OUTPUT_FOLDER+"/samples.js");
		try {
			Scanner s = new Scanner(f);
			String vulnerabilities = null;
			while(s.hasNext()) {
				String line = s.nextLine();
				if(line.equals("var issue_samples = [")) {
					vulnerabilities = "[";
					while(s.hasNext())
						vulnerabilities += s.nextLine();
					break;
				}
					
			}
			s.close();
			
			
			JSONParser parser = new JSONParser();
			try {
				JSONArray vuls = (JSONArray) parser.parse(vulnerabilities.replace("'", "\"").replace(";", ""));
				
				@SuppressWarnings("unchecked")
				Iterator<JSONObject> it = vuls.iterator();
				
				while(it.hasNext()) {
					JSONObject v = (JSONObject) it.next();
					long type = (long) v.get("type");
					String name;
					long severity = (long) v.get("severity");
					JSONArray reqs = (JSONArray) v.get("samples");
					
					boolean show;
					
					if(type==50103) {
						name = "SQL injection scripting vulnerability";
						show = true;
						result.add(new Vulnerability("web.injection", reqs.get(0).toString(), ""));
					}
					else if(type == 40301) {
						name = "Cross-site scripting vulnerability";
						show = true;
						result.add(new Vulnerability("web.xss", reqs.get(0).toString(), ""));
					}
					else {
						name="<unknown>"; 
						show = false;
					}
					
					
					if(show) {
						JSONObject firstSample = (JSONObject) reqs.get(0);
						System.out.println(name);
						System.out.println("Severity: " + severity);
						System.out.println("Url: " + firstSample.get("url"));
						System.out.println();
					}
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println(vulnerabilities);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}



	@Override
	public void setParameter(String parameter, String value) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void getParameter(String parameter) {
		// TODO Auto-generated method stub
		
	}



	public String getResults() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
