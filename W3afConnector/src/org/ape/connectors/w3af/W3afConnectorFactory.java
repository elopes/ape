package org.ape.connectors.w3af;

import org.ape.spi.BlackboxConnector;
import org.ape.spi.BlackboxConnectorFactory;

public class W3afConnectorFactory implements BlackboxConnectorFactory {
	

	@Override
	public String getToolId() {
		return W3afConnector.TOOL_ID;
	}

	@Override
	public BlackboxConnector getToolConnector() {
		return new W3afConnector();
	}

}
