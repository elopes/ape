package org.ape.connectors.w3af;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.ape.commons.Report;
import org.ape.commons.Target;
import org.ape.commons.Vulnerability;
import org.ape.spi.BlackboxConnector;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * --------TOOL INFO--------
 * w3af - Web Application Attack and Audit Framework
 * Version: 1.6.54
 * Revision: 359c05883c - 10 Jun 2015 01:43
 * Author: Andres Riancho and the w3af team.
 * 
 * @author backbox
 *
 */

public class W3afConnector implements BlackboxConnector{
	
	protected static final String TOOL_ID = "w3af";
	
	
	//W3AF
	private static final String EXECUTABLE_PATH = "/home/backbox/Tese/tools/w3af/w3af_console";
	private static final String LOG_PATH = "/home/backbox/Tese/output/w3af.log";
	protected static final String RESULT_PATH = "/home/backbox/Tese/output/w3af.xml";
	
	
	private String payload = null;
	private Target target;
	
	private Report result;

	
    /**
     * Default constructor. 
     */
    public W3afConnector() {
    	target = new Target();
    	result = new Report(TOOL_ID, target);
    }

	public void execute() throws InterruptedException, IOException {
		
		
		File script = ScriptGenerator.genScript(target, RESULT_PATH);
		
		ProcessBuilder pb = new ProcessBuilder(EXECUTABLE_PATH, "-s", script.getAbsolutePath());
		
		//Logging files 
	    pb.redirectOutput(Redirect.to(new File(LOG_PATH)));
	    pb.redirectError(Redirect.to(new File(LOG_PATH)));
	    
	    Process p = pb.start();
	    p.waitFor();
	    
	    readVulnerabilities();
	    
	    
	}
	
	public void readVulnerabilities() {

		try {
			File fXmlFile = new File(RESULT_PATH);
			
			// Ler resultado para o payload
			int len = (int)fXmlFile.length();
			char[] fileBytes = new char[len];
			FileReader fr = new FileReader(fXmlFile);
			fr.read(fileBytes, 0, len);
			fr.close();
			payload = new String(fileBytes);
			result.setPayload(payload);
			
			// Parse para achar as vulnerabilidades
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
		 
			NodeList nList = doc.getElementsByTagName("vulnerability");
		 
			System.out.println("Found " + nList.getLength() + " vulnerabilities");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node n = nList.item(temp);
				
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) n;

					Vulnerability v;
					if(e.hasAttribute("var"))
						v = new Vulnerability(e.getAttribute("name"), e.getAttribute("method"), e.getAttribute("url"), e.getAttribute("var"));
					else
						v = new Vulnerability(e.getAttribute("name"), e.getAttribute("method"), e.getAttribute("url"));
					
					result.addVulnerability(v);
					
				}
			}
			
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void setParameter(String parameter, String value) {
		String[] params = parameter.split("\\.");
		if(params[0].equals("target")) {
			if(params[1].equals("url"))
				target.setTargetUrl(value);
			
			else if(params[1].equals("framework"))
				target.setTargetFramework(value);
			
			else if(params[1].equals("os"))
				target.setTargetOS(value.toLowerCase());
			
		}
	}

	public String getParameter(String parameter) {
		String[] params = parameter.toLowerCase().split("\\.");
		if(params[0].equals("target")) {
			if( params[1].equals("url"))
				return target.getTargetUrl();
			else if(params[1].equals("framework"))
				return target.getTargetFramework();
			else if(params[1].equals("os"))
				return target.getTargetOS();
			else
				return null;
		} 
		else {
			return null;
		}
		
	}

	public Report getReport() {
		return result;
	}

	@Override
	public String getPayload() {
		return payload;
	}

	public String getToolId() {
		return TOOL_ID;
	}

}
