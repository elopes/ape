package org.ape.connectors.w3af;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.ape.commons.Target;

public class ScriptGenerator {
	
	private static final String CRAWL_PLUGINS = "web_spider";
	private static final String AUDIT_PLUGINS = "blind_sqli, csrf, os_commanding, sqli, xss";
	
	
	private static String genTargetScript(Target t) {
		String result = "target\n";
		
		if(t.getTargetFramework() != null) 
			result += "set targetFramework " + t.getTargetFramework() + "\n";
		
		if(t.getTargetOS() != null) 
			result += "set targetOS " + t.getTargetOS() + "\n";
		
		if(t.getTargetUrl() != null)
			result += "set target " + t.getTargetUrl() + "\n";
		
		result += "back\n";
		
		return result;
	}
	
	//TODO: genHttpSettingsScript(), genMiscSettingsScript() (connector developer level)
	
	private static String genPluginsScript(String outputPath) {
		String result = "plugins\n";
		
		//TODO Configure grep, discovery, audit, bruteforce, auth, evasion and mangle plugins (connector developer level)
		
		//Configure crawl plugins
		result += "crawl "+CRAWL_PLUGINS+"\n";
				
		//Configure audit modules
		result += "audit "+ AUDIT_PLUGINS + "\n";
		
		// Configure output
		result += "output console, xml_file\n";
		result += "output config xml_file\n";
		result += "set output_file " + outputPath + "\n";
		result += "back\n";
		
		result += "back\n";
		return result;
	}
	
	
	protected static File genScript(Target t, String outputPath) {
		File tempFile = null;
		
		//Generate script string
		String script = genTargetScript(t);
		script += genPluginsScript(outputPath);
		script+="cleanup\n";
		script+="start\n";
		script+="exit\n";
		
		//Write it to temporary file
		try {
			tempFile = File.createTempFile("w3af-script", ".tmp");

			PrintWriter pw = new PrintWriter(tempFile);
			pw.write(script);
			pw.flush();
			pw.close();
		} catch (IOException e) {
			System.out.println("[!!] W3af connector has encountered an error creating script file");
			e.printStackTrace();
		}
		
		return tempFile;
		
	}
	
	
}
