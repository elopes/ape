package org.ape.commons;

import java.util.LinkedList;
import java.util.List;

public class Report {
	
	private Target target;
	private String toolId;
	private List<Vulnerability> found;
	private String payload;
	
	public Report(String toolId, Target target) {
		super();
		this.toolId = toolId;
		this.target = target;
		this.found = new LinkedList<Vulnerability>();
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public String getToolId() {
		return toolId;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public void addVulnerability(Vulnerability v) {
		this.found.add(v);
	}
	
	public List<Vulnerability> getVulnerabilities() {
		return found;
	}
}
