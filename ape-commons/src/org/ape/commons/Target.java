package org.ape.commons;

public class Target {
	
	private String targetOS;
	private String targetFramework;
	private String target;
	
	
	public Target() {}

	public Target(String targetOS, String targetFramework, String target) {
		super();
		this.targetOS = targetOS;
		this.targetFramework = targetFramework;
		this.target = target;
	}

	public String getTargetOS() {
		return targetOS;
	}
	
	public void setTargetOS(String targetOS) {
		this.targetOS = targetOS;
	}
	
	
	public String getTargetFramework() {
		return targetFramework;
	}
	
	public void setTargetFramework(String targetFramework) {
		this.targetFramework = targetFramework;
	}
	
	
	public String getTargetUrl() {
		return target;
	}
	public void setTargetUrl(String target) {
		this.target = target;
	}
	
	

}
