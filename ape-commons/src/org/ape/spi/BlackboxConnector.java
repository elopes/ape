package org.ape.spi;

import java.io.IOException;

import org.ape.commons.Report;

/**
 * Implementations of this interface must include a no-argument constructor
 * 
 * @author en.lopes
 *
 */
public interface BlackboxConnector {
	
	/**
	 * Sets the given parameter to the given value. This should be done before calling the execute method.
	 * 
	 * @param parameter - the parameter to be set to the specified value
	 * @param value - the value which the specified parameter is to be set to
	 */
	public void setParameter(String parameter, String value);
	
	
	/**
	 * Returns the value to which the specified parameter is set
	 * 
	 * @param parameter - the parameter whose value is to be returned
	 * @return the value to which the specified parameter is set to, or null if the parameter is not set or not supported by the tool
	 */
	public String getParameter(String parameter);
	
	
	/**
	 * Returns the set of found vulnerabilities which must be parsed from the tools output
	 * 
	 * @return set of found vulnerabilities
	 */
	public Report getReport();
	
	
	/**
	 * Returns the raw output of the tool's execution so it can be included in the final report
	 * 
	 * @return raw output of the tool
	 */
	public String getPayload();
	
	
	/**
	 * Returns the id of the tool which is managed by the implementation of this interface.
	 * 
	 * @return
	 */
	public String getToolId();
	
	
	/**
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public void execute() throws InterruptedException, IOException;

}
