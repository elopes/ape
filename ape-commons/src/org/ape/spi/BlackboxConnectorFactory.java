package org.ape.spi;

public interface BlackboxConnectorFactory {
	
	public String getToolId();
	public BlackboxConnector getToolConnector();

}
