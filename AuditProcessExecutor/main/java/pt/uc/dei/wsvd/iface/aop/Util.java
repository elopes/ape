/*******************************************************************************
 *
 * CISUC, Department of Informatics Engineering
 * Faculdade de Ciencias e Tecnologia
 * University of Coimbra
 * Coimbra, Portugal
 *
 * Nuno Manuel dos Santos Antunes
 *
 *******************************************************************************
 *
 * Applying Broad Interface Attack and Monitoring for
 * Vulnerability Detection in Web Services
 *
 *******************************************************************************
 * Last changed on : $Date: 2010-12-01 16:25:26 +0000 (Wed, 01 Dec 2010) $
 * Last changed by : $Author: nmsa $
 ******************************************************************************/
package pt.uc.dei.wsvd.iface.aop;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.aspectj.lang.reflect.SourceLocation;
import pt.uc.dei.wsvd.report.ReportInterface;

/**
 *
 * @author Nuno Antunes <nmsa@dei.uc.pt>
 */
public class Util {

    private static final String WSDV_AOP_CONFIGURATION_SERVICES_PACKAGE = "pt.uc.dei.";
    private static final String WSDV_AOP_CONFIGURATION_AOP_PACKAGE = "pt.uc.dei.wsvd.iface.aop.";
    private static final String WSDV_AOP_CONFIGURATION_ADVICE_TOKEN = "$advice";
    private static final Pattern WSDV_SQL_REGEX_SLASHES = Pattern.compile("\\\\\\\\");
    private static final Pattern WSDV_SQL_REGEX_QUOTES = Pattern.compile("\\\\'");
    private static final Pattern WSDV_SQL_REGEX_ESCAPED = Pattern.compile("''");
    private static final Pattern WSDV_SQL_REGEX_STRING = Pattern.compile("'[^']*'");
    private static final Pattern WSDV_SQL_REGEX_OP = Pattern.compile("_(..)_p", Pattern.CASE_INSENSITIVE);
    private static final Pattern WSDV_SQL_REGEX_PO = Pattern.compile("_(..)_o", Pattern.CASE_INSENSITIVE);
    private static final AtomicInteger reportVersion = new AtomicInteger(0);
    public static final String[] WSDV_AOP_DBMS = {
        "/* mysql-connector-java",
        "SHOW COLLATION"};
    private static ReportInterface reportInterface;

    public static boolean sql(SourceLocation sourceLocation, String sql) {
        boolean systemSQL = false;
        for (String systemToken : Util.WSDV_AOP_DBMS) {
            systemSQL |= sql.contains(systemToken);
        }
        String lineOfCode = getLineOfCode(sourceLocation);
        if (systemSQL) {
            Log.system(lineOfCode + "|" + sql);
            Log.system("SIG[" + getStackSignature(sourceLocation, sql) + "]");
        } else {
            final String parse = parse(sql);
            Matcher matcher = WSDV_SQL_REGEX_OP.matcher(parse);
            if (matcher.find()) {
                String code = matcher.group(1);
                final char o = code.charAt(0);
                final char p = code.charAt(1);
                detected(o, p, sql, lineOfCode, false);
            }
            Matcher reverseMatcher = WSDV_SQL_REGEX_PO.matcher(parse);
            if (reverseMatcher.find()) {
                String code = reverseMatcher.group(1);
                final char p = code.charAt(0);
                final char o = code.charAt(1);
                detected(o, p, sql, lineOfCode, true);
            }
        }
        return !systemSQL;
    }

    private static void detected(char operation, char parameter, String sql, String lineOfCode, boolean reverse) {
        if (reportInterface == null) {
            lookupWSDV(0);
        }
        Log.sql(operation + " " + parameter + " " + lineOfCode + " " + reverse + " " + sql);
        int version = reportVersion.get();
        for (int i = 1; i <= 50; i++) {
            try {
                reportInterface.report(sql, operation, parameter, lineOfCode, reverse);
                Log.trace("Reported: " + sql);
                return;
            } catch (RemoteException re) {
                if (!lookupWSDV(version)) {
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException ex) {
                    }
                    Log.fatal("Retry: " + i + "  :  " + re);
                }
            }
        }
        Log.logar("Not Reported: " + sql);
        Log.fatal("Cannot reconnect... ");
    }

    private static boolean lookupWSDV(int version) {
        synchronized (reportVersion) {
            if (reportVersion.compareAndSet(version, version + 1)) {
                try {
                    Registry registry = LocateRegistry.getRegistry(Configuration.WSDV_SERVER_RMI_HOST, ReportInterface.WSDV_SERVER_RMI_PORT);
                    reportInterface = (ReportInterface) registry.lookup(ReportInterface.WSDV_SERVER_RMI_NAME);
                    Log.fatal("lookupWSDV: " + reportInterface);
                    return reportInterface != null;
                } catch (Exception re) {
                    Log.fatal("Cannot lookup WSDV: " + re);
                }
                return false;
            }
            return true;
        }
    }

    private static String parse(String sql) {
        sql = WSDV_SQL_REGEX_SLASHES.matcher(sql).replaceAll("");
        sql = WSDV_SQL_REGEX_QUOTES.matcher(sql).replaceAll("");
        sql = WSDV_SQL_REGEX_ESCAPED.matcher(sql).replaceAll("");
        sql = WSDV_SQL_REGEX_STRING.matcher(sql).replaceAll("");
        return sql;
    }

    public static String getLineOfCode(final SourceLocation original) {
        String file = original.getFileName();
        int line = original.getLine();
        boolean todo = true;
        StackTraceElement[] st = (new Exception()).getStackTrace();
        for (int i = 0; todo && i < st.length; i++) {
            StackTraceElement ste = st[i];
            final String className = ste.getClassName();
            final String methodName = ste.getMethodName();
            if (className.contains(WSDV_AOP_CONFIGURATION_SERVICES_PACKAGE)
                    && !(className.contains(WSDV_AOP_CONFIGURATION_AOP_PACKAGE) /*||
                    className.contains(WSDV_AOP_CONFIGURATION_SERVICES_VALIDATOR_PACKAGE)*/ || methodName.contains(WSDV_AOP_CONFIGURATION_ADVICE_TOKEN))) {
                file = ste.getFileName();
                line = ste.getLineNumber();
                todo = false;
            }
        }
        return new StringBuilder(file).append(":").append(line).toString();
    }

    public static String getStackSignature(final SourceLocation original, final String prefix) {
        StringBuilder sb = new StringBuilder(prefix).append(original.getFileName()).append(":").append(original.getLine());
        boolean reachedTheLastInstructionEnd = false;
        boolean record = false;
        StackTraceElement[] st = (new Exception()).getStackTrace();
        for (int i = 0; !reachedTheLastInstructionEnd && i < st.length; i++) {
            StackTraceElement ste = st[i];
            final String className = ste.getClassName();
            final String methodName = ste.getMethodName();
            if (!methodName.contains(WSDV_AOP_CONFIGURATION_ADVICE_TOKEN)) {
                if (record) {
                    if (className.contains(WSDV_AOP_CONFIGURATION_SERVICES_PACKAGE)
                            && !(className.contains(WSDV_AOP_CONFIGURATION_AOP_PACKAGE) /*||
                            className.contains(WSDV_AOP_CONFIGURATION_SERVICES_VALIDATOR_PACKAGE)*/)) {
                        reachedTheLastInstructionEnd = true;
                    }
                    sb.append(";").append(ste.getFileName()).append(":").append(ste.getLineNumber());
                } else if (className.equals(Util.class.getName())) {
                    record = true;
                }
            }
        }
        return sb.toString();
    }
}
