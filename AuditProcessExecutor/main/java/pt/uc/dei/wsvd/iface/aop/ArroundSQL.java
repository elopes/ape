/*******************************************************************************
 *
 * CISUC, Department of Informatics Engineering
 * Faculdade de Ciencias e Tecnologia
 * University of Coimbra
 * Coimbra, Portugal
 *
 * Nuno Manuel dos Santos Antunes
 *
 *******************************************************************************
 *
 * Applying Broad Interface Attack and Monitoring for
 * Vulnerability Detection in Web Services
 *
 *******************************************************************************
 * Last changed on : $Date: 2010-12-01 16:25:26 +0000 (Wed, 01 Dec 2010) $
 * Last changed by : $Author: nmsa $
 ******************************************************************************/
package pt.uc.dei.wsvd.iface.aop;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import static pt.uc.dei.wsvd.iface.aop.Util.sql;

/**
 *
 *
 *
 * 
 * @author Nuno Antunes <nmsa@dei.uc.pt>
 */
@Aspect
public class ArroundSQL {

    /**
     *
     *
     *
     */
    @Around("execution(* java.sql.Statement.executeQuery(..)) || "
    + "execution(* java.sql.Statement.executeUpdate(..)) || "
    + "execution(* java.sql.Statement.execute(..)) || "
    + "execution(* java.sql.Connection.prepareStatement(..)) ")
    public Object aroundSQLInvocation(ProceedingJoinPoint pjp) throws Throwable {
        String sql = "NO SQL";
        try {
            sql = (String) pjp.getArgs()[0];
            if (!sql(pjp.getSourceLocation(), sql)) {
                return pjp.proceed();
            }
            Object proceed = pjp.proceed();
//            if (proceed instanceof ResultSet) {
//                Log.logar(toString(sql, proceed));
//            } else if (proceed instanceof Integer) {
//                Log.logar("Integer : " + proceed);
//            } else {
//                Log.logar("else : " + proceed);
//            }
            return proceed;
        } catch (Throwable throwable) {
            try {
                Log.trace("EXCEPCAOLOGED: ## " + throwable.toString());
                Log.trace("Caused by: ## " + sql);
            } catch (Exception e) {
                Log.logar("Exception DURING Exception LOGED: ## " + e);
            }
            throw throwable;
        }
    }

    private String toString(String sql, Object proceed) throws SQLException {
        StringBuilder sb = new StringBuilder("ResultSet [" + sql + "]: \n");
        ResultSet rs = (ResultSet) proceed;
        ResultSetMetaData metaData = rs.getMetaData();
        final int columnCount = metaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            sb.append(metaData.getColumnName(i)).append("(type").append(metaData.getColumnType(i)).append(")\t");
        }
        sb.append("\n");
        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                sb.append(rs.getString(1)).append(" || \t");
            }
            sb.append("\n");
        }
        sb.append("------------------------\n");
        return sb.toString();
    }
}
