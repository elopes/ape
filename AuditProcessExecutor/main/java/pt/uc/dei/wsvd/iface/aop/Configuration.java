/*******************************************************************************
 *
 * CISUC, Department of Informatics Engineering
 * Faculdade de Ciencias e Tecnologia
 * University of Coimbra
 * Coimbra, Portugal
 *
 * Nuno Manuel dos Santos Antunes
 *
 *******************************************************************************
 *
 * Web Services Vulnerability Detection
 * Tools and Methodologies
 *
 *******************************************************************************
 * Last changed on : $Date: 2010-11-29 14:01:44 +0000 (Mon, 29 Nov 2010) $
 * Last changed by : $Author: nmsa $
 ******************************************************************************/
package pt.uc.dei.wsvd.iface.aop;

/**
 *
 * A interface to AOP module to notify the tester tool for a finded vulnerability.
 *
 *
 *
 * @see WSDVServer
 *
 *
 *
 * @since   r1169
 * @version $Revision: 1199 $
 *
 * @author  $Author: nmsa $
 */
public final class Configuration {

    /**
     * Modify to match the host where the tool is running
     */
    public static String WSDV_SERVER_RMI_HOST = "localhost";
    public static final String WSDV_LOGGING_DIR = "C:/wsdv-iface/logging/";
}
