/*******************************************************************************
 *
 * CISUC, Department of Informatics Engineering
 * Faculdade de Ciencias e Tecnologia
 * University of Coimbra
 * Coimbra, Portugal
 *
 * Nuno Manuel dos Santos Antunes
 *
 *******************************************************************************
 *
 * Applying Broad Interface Attack and Monitoring for
 * Vulnerability Detection in Web Services
 *
 *******************************************************************************
 * Last changed on : $Date: 2010-12-02 18:11:14 +0000 (Thu, 02 Dec 2010) $
 * Last changed by : $Author: nmsa $
 ******************************************************************************/
package pt.uc.dei.wsvd.iface.aop;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @since   r1169
 * @version $Revision: 1213 $
 *
 * @author  $Author: nmsa $
 */
public class Log {

    private static final String TIME = Long.toString(System.currentTimeMillis());
    private static final DateFormat df = new SimpleDateFormat("[yy/MM/dd HH:mm:ss]");
    private static final String PREFIX = TIME + "-wsdviface-";
    private static final String SUFIX = ".txt";
    private static final String WSDV_AOP_LOGGING_FILE_TRACE = Configuration.WSDV_LOGGING_DIR + PREFIX + "trace" + SUFIX;
    private static final String WSDV_AOP_LOGGING_FILE_LOG = Configuration.WSDV_LOGGING_DIR + PREFIX + "log" + SUFIX;
    private static final String WSDV_AOP_LOGGING_FILE_SQL = Configuration.WSDV_LOGGING_DIR + PREFIX + "sql" + SUFIX;
    private static final String WSDV_AOP_LOGGING_FILE_SYSTEM = Configuration.WSDV_LOGGING_DIR + PREFIX + "system" + SUFIX;
    private static final String WSDV_AOP_LOGGING_FILE_FATAL = Configuration.WSDV_LOGGING_DIR + PREFIX + "fatal" + SUFIX;
    private static PrintWriter logFile;
    private static PrintWriter traceFile;
    private static PrintWriter sqlFile;
    private static PrintWriter systemFile;
    private static PrintWriter fatalFile;

    static {
        try {
            logFile = new PrintWriter(new FileOutputStream(WSDV_AOP_LOGGING_FILE_LOG));
            traceFile = new PrintWriter(new FileOutputStream(WSDV_AOP_LOGGING_FILE_TRACE));
            sqlFile = new PrintWriter(new FileOutputStream(WSDV_AOP_LOGGING_FILE_SQL));
            systemFile = new PrintWriter(new FileOutputStream(WSDV_AOP_LOGGING_FILE_SYSTEM));
            fatalFile = new PrintWriter(new FileOutputStream(WSDV_AOP_LOGGING_FILE_FATAL));
        } catch (Exception e) {
            System.out.println("Cannot open files... " + e);
            e.printStackTrace();
        }
    }

    public static void trace(String logItem) {
        String log = df.format(new Date()) + logItem;
        traceFile.println(log);
        traceFile.flush();
    }

    public static void logar(String log) {
        System.out.println("[LOG]" + log);
        logFile.println(log);
        logFile.flush();
        trace(log);
    }

    public static void sql(String sql) {
        sqlFile.println(sql);
        sqlFile.flush();
        trace(sql);
    }

    public static void system(String sql) {
        systemFile.println(sql);
        systemFile.flush();
        trace(sql);
    }

    public static void fatal(String log) {
        System.out.println("\n|||FATAL||||: " + log);
        fatalFile.println(log);
        fatalFile.flush();
    }
}
