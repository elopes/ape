/*******************************************************************************
 *
 * CISUC, Department of Informatics Engineering
 * Faculdade de Ciencias e Tecnologia
 * University of Coimbra
 * Coimbra, Portugal
 *
 * Nuno Manuel dos Santos Antunes
 *
 *******************************************************************************
 *
 * Web Services Vulnerability Detection
 * Tools and Methodologies
 *
 *******************************************************************************
 * Last changed on : $Date: 2010-11-29 14:01:44 +0000 (Mon, 29 Nov 2010) $
 * Last changed by : $Author: nmsa $
 ******************************************************************************/
package pt.uc.dei.wsvd.report;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * A interface to AOP module to notify the tester tool for a finded vulnerability.
 *
 *
 *
 * @see WSDVServer
 *
 *
 *
 * @since   r1169
 * @version $Revision: 1199 $
 *
 * @author  $Author: nmsa $
 */
public interface ReportInterface extends Remote {

    public static String WSDV_SERVER_RMI_NAME = "WSDV_SERVER_RMI_NAME";
    public static int WSDV_SERVER_RMI_PORT = 5008;

    public void report(String sql, char operation, char parameter, boolean reverse) throws RemoteException;

    public void report(String sql, char operation, char parameter, String lineOfCode, boolean reverse) throws RemoteException;
}
