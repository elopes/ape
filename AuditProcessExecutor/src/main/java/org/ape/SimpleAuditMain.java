package org.ape;

import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;

public class SimpleAuditMain {

	public static void main(String[] args) { 

		String bpmnPath = "";
		String processId = "";
		
		String outputType = "";
		String outputPath = "";
		
		if(args.length == 1) {
			printUsage();
			System.exit(0);
		}
		
		bpmnPath = args[0];
		System.out.println("bpmnPath = " + bpmnPath);
		
		for(int i = 1; i < args.length; i++) {
			
			/*
			 * Print usage instructions
			 */
			if(args[i].equals("-h")) {
				printUsage();
				System.exit(0);
			}
			
			/*
			 * Set output format (xml, console)
			 */
			else if(args[i].equals("-o") || args[i].equals("--output")) {
				outputType = args[i+1];
				System.out.println("outputType = " + outputType);
				i++;
			}
			
			/*
			 * Set output file
			 */
			else if(args[i].equals("-f") || args[i].equals("--file")) {
				outputPath = args[i+1];
				System.out.println("outputPath = " + outputPath);
				i++;
			}
			
			/*
			 * Process Id
			 */
			else if(args[i].equals("-p") || args[i].equals("--process")) {
				processId = args[i+1];
				System.out.println("processId = " + processId);
				i++;
			}
		}
		
		
		// Verify parameter invariants
		if(bpmnPath.length() == 0) {
			printUsage();
			System.exit(0);
		}
		
		if(processId.length() == 0) {
			System.err.println("[!!] ERROR: Please specify the ID of the process you want to run, using -p,--process");
		}
		
		if(outputType.equals("xml") && outputPath.length() == 0) {
			System.err.println("[!!] ERROR: You chose XML as your output format. Please specify the output file, using -o,--output");
			
		}

		
		//Execute process
		KieHelper kieHelper = new KieHelper();

		KieBase kieBase = kieHelper.addResource(
				ResourceFactory.newClassPathResource(bpmnPath))
				.build();

		KieSession ksession = kieBase.newKieSession();

		ksession.getWorkItemManager().registerWorkItemHandler("BlackboxTool",
				new BlackboxToolWorkItemHandler());

		ksession.startProcess(processId);
		ksession.dispose();
		
		// Output result
		ReportRepository repo = ReportRepository.getInstance();
		repo.exportReport(outputType, outputPath);
		
	}

	private static void printUsage() {
		//TODO ADD PARAMETERS FOR CHOSING THE PROCESS AND BPMN FILE
		System.out.println("APE - Audit Process Executor");
		System.out.println("./ape [bpmn file] [options]");
		System.out.println();
		System.out.println("Options: ");
		System.out.println("-p,--process [process]\t\t process ID");
		System.out.println("-o,--output [type]\t\t output type (xml, console)");
		System.out.println("-f,--file [file]\t\t output file path");
		
	}

}
