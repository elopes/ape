package org.ape;



import java.io.IOException;

import org.ape.spi.BlackboxConnector;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;


public class BlackboxToolWorkItemHandler implements WorkItemHandler {

	
	//---------PARAMETER NAMES--------------
	private static final String TOOL_ID = "tool";
	private static final String TARGET_URL = "target.url";
	private static final String TARGET_FRAMEWORK = "target.framework";
	private static final String TARGET_OS = "target.os";
	

	private BlackboxToolService bts;
	private ReportRepository reports;
	
	public BlackboxToolWorkItemHandler() {
		this.bts = BlackboxToolService.getInstance();
		this.reports = ReportRepository.getInstance();
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		//Call the tool from the connector
		String toolId = (String) workItem.getParameter(TOOL_ID);
		
		//Get target info
		String targetUrl = (String) workItem.getParameter(TARGET_URL);
		String targetFramework = (String) workItem.getParameter(TARGET_FRAMEWORK);
		String targetOs = (String) workItem.getParameter(TARGET_OS);

		System.out.println("[**] Starting scan with " + toolId + "...");
		
		BlackboxConnector tool = bts.getBlackboxConnector(toolId);
		
		tool.setParameter(TARGET_URL, targetUrl);
		tool.setParameter(TARGET_FRAMEWORK, targetFramework);
		tool.setParameter(TARGET_OS, targetOs);
		
		System.out.println("\tURL: " + targetUrl);
		System.out.println("\tFramework: " + targetFramework);
		System.out.println("\tOS: " + targetOs);
		
		try {
			tool.execute();
			reports.commit(workItem.getId(), tool.getReport());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		manager.completeWorkItem(workItem.getId(), null);
	}

	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		// TODO Auto-generated method stub

	}


}
