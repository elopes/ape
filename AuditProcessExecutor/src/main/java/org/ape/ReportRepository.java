package org.ape;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.ape.commons.Report;
import org.ape.reporting.*;
import org.ape.commons.Vulnerability;

public class ReportRepository {
	
	private static ReportRepository repo;
	private Map<Long, Report> vulnById;
	
	private ReportRepository() {
		vulnById = new HashMap<Long, Report>();
	}
	
	public static ReportRepository getInstance() {
		if(repo == null)
			repo = new ReportRepository();
		return repo;
	}
	
	
	public void commit(Long workItem, Report r) {
		vulnById.put(workItem, r);
		System.out.println("[*] " + r.getToolId() + " has reported " + r.getVulnerabilities().size() + " vulnerabilities.");
		//printVulns();
	}
	
	private void printVulns() {
		//TODO Meter os prints bonitos
		int counter = 0;
		for(Report r : vulnById.values()) {
			System.out.println("----------" + r.getToolId() +  "Report----------");
			for(Vulnerability v : r.getVulnerabilities()) {
				System.out.println("Vulnerability no."+counter);
				System.out.println("ID: " + v.getId());
				System.out.println("URL: " + v.getUrl());
				System.out.println("Method: " + v.getMethod());
				counter++;
			}
			counter = 0;
		}
	}
	
	public void exportReport(String format, String path) {
		if(format.equals("xml")) {
			exportXml(path);
			
		} else if(format.equals("console")) {
			printVulns();
		}
	}

	private void exportXml(String path) {
		XmlReporter reporter = new XmlReporter();
		reporter.report(new File(path), repo.vulnById);
	}

}
