package org.ape.reporting;

import java.io.File;
import java.util.Map;

import org.ape.commons.Report;

public interface Reporter {
	
	public void report(File f, Map<Long, Report> vuln);

}
