package org.ape.reporting;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.ape.commons.Report;
import org.ape.commons.Vulnerability;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlReporter implements Reporter {
	
	static final String JAXP_SCHEMA_LANGUAGE =
		    "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
		static final String W3C_XML_SCHEMA =
		    "http://www.w3.org/2001/XMLSchema";


	@Override
	public void report(File f, Map<Long, Report> vuln) {

		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setValidating(true);
			docFactory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();
			Element root = doc.createElement("process-report");
			root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.setAttribute("xsi:noNamespaceSchemaLocation", "report.xsd");
			
			//Reports
			Report curReport;
			for(Entry<Long, Report> e : vuln.entrySet()) {
				curReport = e.getValue();
				
				// Report Elements
				Element report = doc.createElement("report");
				
				// Attributes
				report.setAttribute("task", ""+e.getKey());
				report.setAttribute("tool", curReport.getToolId());
				
				// Target
				Element target = doc.createElement("target");
				Element url = doc.createElement("url");
				url.setTextContent(curReport.getTarget().getTargetUrl());
				Element os = doc.createElement("os");
				os.setTextContent(curReport.getTarget().getTargetOS());
				Element framework = doc.createElement("framework");
				framework.setTextContent(curReport.getTarget().getTargetFramework());
				target.appendChild(url);
				target.appendChild(os);
				target.appendChild(framework);
				report.appendChild(target);
				
				// Vulnerabilities
				Element vulnerabilities = doc.createElement("vulnerabilities");
				for(Vulnerability v : curReport.getVulnerabilities()) {
					Element vulnerability = doc.createElement("vulnerability");
					Element id = doc.createElement("id");
					id.setTextContent(v.getId());
					Element method = doc.createElement("method");
					method.setTextContent(v.getMethod());
					Element localUrl = doc.createElement("url");
					localUrl.setTextContent(v.getMethod());
					vulnerability.appendChild(id);
					vulnerability.appendChild(method);
					vulnerability.appendChild(url);
					if(v.getVar() != null) {
						Element var = doc.createElement("variable");
						var.setTextContent(v.getVar());
						vulnerability.appendChild(var);
					}
					vulnerabilities.appendChild(vulnerability);
				}
				report.appendChild(vulnerabilities);
				
				//Payload
				Element payload = doc.createElement("payload");
				payload.setTextContent(curReport.getPayload());
				report.appendChild(payload);
				
				root.appendChild(report);
			}
			doc.appendChild(root);
			//doc.
			writeToFile(doc, f);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	private static void writeToFile(Document doc, File f) {
		Transformer transformer;
		doc.normalize();
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
	        DOMSource source = new DOMSource(doc);
	        StreamResult console = new StreamResult(f);
	        transformer.transform(source, console);
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println();
	}
	
	

}
