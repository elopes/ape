package org.ape;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

import org.ape.spi.BlackboxConnector;
import org.ape.spi.BlackboxConnectorFactory;

public class BlackboxToolService {
	
	private static BlackboxToolService service;
	private ServiceLoader<BlackboxConnectorFactory> loader;
	
	private BlackboxToolService() {
		loader = ServiceLoader.load(BlackboxConnectorFactory.class);
	}
	
	public static synchronized BlackboxToolService getInstance() {
        if (service == null) {
            service = new BlackboxToolService();
        }
        return service;
    }
	
	public BlackboxConnector getBlackboxConnector(String toolId) {
		try {
            Iterator<BlackboxConnectorFactory> tools = loader.iterator();
            while (tools.hasNext()) {
                BlackboxConnectorFactory d = tools.next();
                if(toolId.equals(d.getToolId()))
					return d.getToolConnector();
            }
        } catch (ServiceConfigurationError serviceError) {
            serviceError.printStackTrace();
        }
		return null;
	}
	

}
